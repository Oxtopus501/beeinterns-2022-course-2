let titlesArray = document.querySelectorAll('.lection__title');
for (let i = 0; i < 4; i++) {
	const title = titlesArray[i];
	title.textContent = title.textContent.toUpperCase();
}

let subtitlesArray = document.querySelectorAll('.lection__subtitle');
for (let i = 0; i < 4; i++) {
	const subtitle = subtitlesArray[i];
	if (subtitle.textContent.length > 20) {
		subtitle.textContent = `${subtitle.textContent.substring(0, 20)}...`;
	}
}